﻿using NUnit.Framework;
using PankkitiliSovellus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkitiliSovellusTests
{
    [TestFixture]
    public class PankkitiliTest
    {
        /* Pankkotilisovellus sisältää
         * 
         * Luokka, jonka nimi on Pankkitili.
         * Pankkitilissä on kolme toimintoa.
         * 
         * -Tallettaa.
         * -Nostaa rahaa.
         * -- Tarkistaa ettei tili mene miinukselle.
         * - Siirtää rahaa tililtä toiselle
         * -- Tarkistaa ettei tili mene miinukselle.
         * 
         * 
          */

        public Pankkitili tili1 = null;


        // OneTimeSetup
        // OneTimeTearDown
        // TearDown
        [SetUp]
        public void TestienAlustaja()
        {
            this.tili1 = new Pankkitili(100);
        }


        [Test]
        public void LuoPankkitili()
        {
            

            // Testataan olion luokan tyyppi
            Assert.IsInstanceOf<Pankkitili>(tili1);

        }

        [Test]
        public void AsetaPankkiTililleAlkusaldo()
        {
            

            // Testataan arvon yhtäsuuruutta
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void TalletaRahaaPankkitilille()
        {
            
            // Tallennetaan rahaa tilille
            tili1.Talleta(250);

            // Testataan arvon yhtäsuuruutta
            Assert.That(350, Is.EqualTo(tili1.Saldo));
        }
        [Test]
        public void NostaPankkitililtäRahaa()
        {

            tili1.NostaRahaa(75);

            // Testataan arvon yhtäsuuruutta
            Assert.That(25, Is.EqualTo(tili1.Saldo));
        }
        [Test]
        public void NostonJalkeenPankkitiliEiVoiOllaMiinuksella()
        {
            // Testataan antaako ohjelma halutun virhetyypin

            Assert.Throws<ArgumentException>(() => tili1.NostaRahaa(175));
            // Vaikka virhe sattuu niin rahoja ei menetetä.
            // Pitäisi olla loppusaldon kanssa sama.
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }


    }

}
